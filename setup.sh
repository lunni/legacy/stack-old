#!/bin/bash
# shellcheck disable=SC2016

set -euo pipefail

fb="$(tput bold)"
fn="$(tput sgr0)"

log() {
  echo "$1" > /dev/stderr
}

log 'Lunni Installer v2'
log ''

if ! command -v 'openssl' > /dev/null; then
  log 'You have to install `openssl` to continue!'
  log 'It should be readily available in your OS package manager.'
  exit 1
fi

if ! command -v 'docker' > /dev/null; then
  log 'You have to install Docker and set up a swarm before running this script!'
  log 'Follow the instructions at <https://docs.docker.com/get-docker/>.'
  log ''
  log 'Stuck? Try <https://dockerswarm.rocks/> for an easy guide :-)'
  exit 1
fi

# Workaround for https://github.com/docker/cli/issues/2910:
if ! docker info &> /dev/null; then
  log "Looks like you don't have access to the Docker socket."
  log "Make sure you're in the docker group or run this script as root."
  exit 1
fi

if [ "$(docker info --format '{{.Swarm.ControlAvailable}}')" = 'false' ]; then
  log 'You have to set up a Docker Swarm before running this script!'
  log 'Simple `docker swarm init` should do the trick.'
  log ''
  log 'Stuck? Try <https://dockerswarm.rocks/> for an easy guide :-)'
  exit 1
fi

if [[ -f 'lunni.cfg' ]]; then
  log 'Using saved configuration from lunni.cfg.'
  log ''
  source 'lunni.cfg'
  cat 'lunni.cfg' > 'lunni.cfg~'
fi

echo "# created on $(date)" > lunni.cfg

log "Welcome to the Lunni installer! We'll set up a Docker stack consisting of:"
log ''
log '- Traefik, a reverse proxy (https://traefik.io/),'
log '- and Portainer, a Docker management UI (https://www.portainer.io/).'
log ''
log 'Think of something like a tiny Heroku on your own server, but running'
log 'services from standard docker-compose.yml files.'
log ''
log 'To get started, we need to ask you some questions. Nothing serious,'
log 'but if you want to bail out, just press Ctrl-C at any time.'
log ''

log "Let's start with your server's domain name. Your admin services"
log 'will run on {traefik,portainer}.[your domain], but your apps'
log 'will be able to run on whatever domains you want.'
log ''
read -p "${fb}Server Domain:${fn} " -rei "${DOMAIN:-$(hostname -f)}" DOMAIN; export DOMAIN
echo "DOMAIN='${DOMAIN}'" >> lunni.cfg
log ''

log 'Great! Now we need your email. It would be used for the HTTPS'
log 'certificate renewal reminders (in case automatic renewal fails'
log 'for any reason).'
log ''
log "P. S. Certificates are generated for free by the awesome guys @ Let's"
log 'Encrypt, please check out <https://letsencrypt.org/privacy/> (and'
log 'maybe also donate to them? :-)'
log ''
read -p "${fb}Email:${fn} " -rei "${EMAIL:-}" EMAIL; export EMAIL
echo "EMAIL='${EMAIL}'" >> lunni.cfg
log ''

log "We'll need to authenticate you somehow. It will be only used"
log 'for Traefik; for Portainer, you will be able to create'
log 'as many users as you need just a bit later.'
log ''
read -p "${fb}Username:${fn} " -rei "${USERNAME:-admin}" USERNAME; export USERNAME
echo "USERNAME='${USERNAME}'" >> lunni.cfg

if [[ "${HASHED_PASSWORD:-}" == "" ]]; then
  HASHED_PASSWORD="$(openssl passwd -apr1)"; export HASHED_PASSWORD
else
  log "${fb}Password:${fn} (reusing \$HASHED_PASSWORD)"
  export HASHED_PASSWORD
fi
echo "HASHED_PASSWORD='${HASHED_PASSWORD}'" >> lunni.cfg
log ''

log "Great! We're ready to set up your Lunni box."
log ''
read -rp 'Press [ENTER] to continue or Ctrl-C to cancel.'
log ''

curl -fsSL 'https://gitlab.com/lunni/lunni/raw/master/stack-v2.yml' -o lunni.yml

node_id="$(docker info -f '{{.Swarm.NodeID}}')"
docker node update --label-add traefik-public.traefik-public-certificates=true "${node_id}"
docker node update --label-add portainer.portainer-data=true "${node_id}"

docker network inspect traefik-public &>/dev/null || docker network create --driver=overlay traefik-public
docker stack deploy -c lunni.yml lunni

log ''
log "Almost done! Please go to <https://portainer.${DOMAIN}/>"
log "and create your Portainer admin user."
log ''
log "Then head over to https://lunni.ale.sh/docs/ to deploy your first app :)"
